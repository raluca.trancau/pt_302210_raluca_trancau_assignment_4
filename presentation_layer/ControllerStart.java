package presentation_layer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import business_layer.Restaurant;
import data_layer.RestaurantSerializator;
/**
 * � contine trei variabile instanta : un obiect de tipul ViewStart, ce reprezinta fereastra din UI, un obiect de tipul Restaurant si un obiect de tipul RestaurantSerializator, care se ocupa cu serializarea datelor din restaurant; clasa are un constructor si o clasa interna care implementeaza interfata ActionListener, clasa ce descrie comportamentul combo box-ului din fereastra de start : Clasa ActionListenerComboBox;
 * 
 *
 */
public class ControllerStart {
	private ViewStart view;
	private Restaurant restaurant;
	private RestaurantSerializator serializator;

	public ControllerStart(ViewStart view, Restaurant restaurant, RestaurantSerializator serializator) {
		super();
		this.view = view;
		this.view.addListenerComboBox(new ActionListenerComboBox());
		this.restaurant = restaurant;
		this.serializator = serializator;
	}

	public class ActionListenerComboBox implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String nextView = view.getComboBox().getSelectedItem().toString();
			if (nextView.equals("Administrator")) {
				view.setVisible(false);
				ViewAdministratorGUI viewAdministrator = new ViewAdministratorGUI();
				ControllerAdministratorGUI controller = new ControllerAdministratorGUI(viewAdministrator, restaurant,
						serializator);
			}
			if (nextView.equals("Waiter")) {
				view.setVisible(false);
				ViewWaiterGUI viewWaiter = new ViewWaiterGUI();
				ControllerWaiterGUI controller = new ControllerWaiterGUI(viewWaiter, restaurant, serializator);
			}
			if (nextView.equals("Chef")) {
				ViewChefGUI viewChef = new ViewChefGUI();
				ControllerChefGUI controller = new ControllerChefGUI(viewChef, restaurant);
			}
		}
	}
}
