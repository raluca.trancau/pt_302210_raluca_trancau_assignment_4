package presentation_layer;

import business_layer.Restaurant;
/**
 * � implementeaza interfata Observer; contine o variabila instanta de tipul ViewChefGUI, care reprezinta fereastra pentru bucatar, si o variabila de tip Restaurant care permite accesul bucatarului la comenzile preluate de chelner; pe langa constructor, clasa descrie corpul metodei update() din interfata, ce are rolul de a actualiza tabela de comenzi de care trebuie sa tina cont bucatarul; pentru a putea urmari efectul design pattern-ului Observer, este necesara deschiderea ferestrei pentru bucatar si mentinerea ei deschisa inainte de a vizualiza, adauga, sterge sau modifica orice comanda in fereastra pentru chelner;
 * 
 *
 */
public class ChefObserver implements Observer {
	
	ViewChefGUI view;
	Restaurant restaurant;
	
	public ChefObserver(ViewChefGUI view, Restaurant restaurant) {
		this.view = view;
		this.restaurant = restaurant;
		restaurant.getObservers().add(this);
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		view.getModel().setRowCount(0);
		Object[][] table = new Object[10000][];

		table = restaurant.viewOrders();
		for (int i = 0; i < table.length; i++) {
			view.getModel().addRow(table[i]);
		}
	}

}
