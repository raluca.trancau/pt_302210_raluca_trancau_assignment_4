package presentation_layer;

import business_layer.Restaurant;
/**
 * � contine trei variabile instanta : un obiect de tipul ViewChefGUI, ce reprezinta fereastra din UI, un obiect de tipul Restaurant si un obiect de tipul ChefObserver, care este notificat de fiecare data cand trebuie sa se afiseze in interfata comenzile introduse de chelner; de asemenea, clasa contine un constructor;
 * 
 *
 */
public class ControllerChefGUI{
	private ViewChefGUI view;
	private Restaurant restaurant;
	private ChefObserver observer;

	public ControllerChefGUI(ViewChefGUI view, Restaurant restaurant) {
		super();
		this.view = view;
		this.restaurant = restaurant;
		this.observer = new ChefObserver(view, restaurant);
	}

}
