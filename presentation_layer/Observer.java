package presentation_layer;

import java.io.Serializable;
/**
 * � este o interfata ce implementeaza interfata Serializable; contine metoda abstracta update() ( folosita la implementarea design pattern-ului Observer); 
 * 
 *
 */
public interface Observer extends Serializable {

	public abstract void update();
}
