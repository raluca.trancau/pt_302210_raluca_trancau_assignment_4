
package data_layer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import business_layer.Restaurant;
/**
 * – contine o metoda care are rolul de a serializa datele cu privire la un obiect de tipul Restaurant, starea obiectului fiind stocata in fisierul “restaurant.ser”;
 * 
 *
 */
public class RestaurantSerializator {
	public void serialize(Restaurant restaurant) {
		try {
			FileOutputStream f = new FileOutputStream("restaurant.ser");
			ObjectOutputStream out = new ObjectOutputStream(f);
			out.writeObject(restaurant);
			out.close();
			f.close();
		} catch (FileNotFoundException exp) {
			exp.printStackTrace();
		} catch (IOException exp) {
			exp.printStackTrace();
		}
	}
}
