package data_layer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
/**
 * � contine o metoda care are rolul de a crea un fisier si de a scrie informatiile necesare facturii in acesta;
 * 
 *
 */
public class FileWriterClass {
	public void write(String fileName, String data) {
		try {
			FileWriter fileWr = new FileWriter(fileName);
			PrintWriter printWr = new PrintWriter(fileWr);
			printWr.print(data);
			printWr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
