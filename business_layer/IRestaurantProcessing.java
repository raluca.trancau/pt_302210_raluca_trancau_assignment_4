package business_layer;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
/**
 * � in aceasta interfata sunt declarate metodele de adaugare, stergere si editare a numelui/pretului unui produs, de creare si stergere a unei comenzi, de calculare a pretului total al unei comenzi si de generare a facturii pentru o comanda;
 * 
 *
 */
public interface IRestaurantProcessing {
	/**
	 * @param menuItem
	 * @pre menuItem.getMenuItemID()>0 && menuItem.getMenuItemName()!=null &&
	 *      menuItem.computePrice()!=0
	 * @pre @forall i : [0 .. getMenu().getSize()-1] @
	 *      (getMenu().get(i).getMenuItemID()!=menuItem.getMenuItemID())
	 * @post getMenu().size() == getMenu().size()@pre + 1
	 */
	public void addNewMenuItem(MenuItem menuItem);

	/**
	 * @param ID
	 * @pre ID > 0
	 * @pre getMenu().size() > 0
	 * @pre @exists i : [0 .. getMenu().size()-1] @ (getMenu().get(i).getMenuItemID() == ID)
	 * @post getMenu().size() == getMenu().size()@pre - 1
	 */
	public void deleteMenuItem(int ID);

	/**
	 * @param ID
	 * @param newName
	 * @pre ID > 0 && newName != null
	 * @pre getMenu().size() > 0
	 * @pre @exists i : [0 .. getMenu().size()-1] @ (getMenu().get(i).getMenuItemID() == ID)
	 * @pre @forall i : [0 .. getMenu().getSize()-1] @
	 *      (!getMenu().get(i).getMenuItemName().equals(newName))
	 * @post @exists i : [0 .. getMenu().size()-1] @ (getMenu().get(i).getMenuItemID() ==
	 *       ID) && (getMenu().get(i).getMenuItemName().equals(newName))
	 */
	public void editMenuItemName(int ID, String newName);

	/**
	 * @param ID
	 * @param newPrice
	 * @pre ID > 0 && newPrice > 0
	 * @pre @exists i : [0 .. getMenu().size()-1] @ (getMenu().get(i).getMenuItemID() == ID)
	 * @pre @forall i : [0 .. getMenu().getSize()-1] @
	 *      (getMenu().get(i).getMenuItemPrice()!=newPrice)
	 * @post @exists i : [0 .. getMenu().size()-1] @ (getMenu().get(i).getMenuItemID() ==
	 *       ID) && (getMenu().get(i).computePrice() == newPrice)
	 */
	public void editMenuItemPrice(int ID, float newPrice);

	/**
	 * @param orderID
	 * @param date
	 * @param table
	 * @param items
	 * @pre orderID > 0 && date != null && table > 0 && items != null
	 * @pre @forall e : [getOrders().entrySet()] @ (e.getKey().getOrderID() !=
	 *      orderID)
	 * @post getOrders().size() == getOrders().size()@pre + 1
	 * @post getOrders().containsKey(new Order(orderID,date,table))
	 */
	public void createNewOrder(int orderID, Date date, int table, List<MenuItem> items);

	/**
	 * @param order
	 * @pre getOrders().size() > 0
	 * @pre @exists e : [getOrders().entrySet()] @ (e.getKey().getOrderID() == order.getOrderID)
	 * @post getOrders().size() == getOrders().size()@pre - 1
	 */
	public void deleteOrder(Order order);

	/**
	 * @param order
	 * @pre @exists e : [getOrders().entrySet()] @ (e.getKey().getOrderID() == order.getOrderID)
	 * @post @result > 0
	 */
	public float computePriceForOrder(Order order);

	/**
	 * @param fileName
	 * @param order
	 * @pre fileName!=null && order.getOrderID() > 0
	 * @pre @exists e : [getOrders().entrySet()] @ (e.getKey().getOrderID() == order.getOrderID)
	 * @post @nochange
	 */
	public void generateBill(String fileName, Order order);
}