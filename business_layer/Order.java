package business_layer;

import java.io.Serializable;
import java.util.Date;
/**
 * � implementeaza interfata Serializable; contine trei variabile instanta: id-ul comenzii (int), data (Date) si numarul mesei (int); pe langa constructor, clasa contine metode de get si set pentru variabilele instanta si suprascrie metodele toString(), hashCode() si equals(Object);
 *
 *
 */
public class Order implements Serializable {
	private int orderID;
	private Date date;
	private int table;

	public Order(int orderID, Date date, int table) {
		super();
		this.orderID = orderID;
		this.date = date;
		this.table = table;
	}

	public int getOrderID() {
		return orderID;
	}

	public Date getDate() {
		return date;
	}

	public int getTableNumber() {
		return table;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setTableNumber(int table) {
		this.table = table;
	}

	@Override
	public int hashCode() {
		int hashCodeCalc = 0;
		hashCodeCalc = hashCodeCalc << 3 | (new Integer(orderID).hashCode() + date.hashCode() + new Integer(table).hashCode());
		return hashCodeCalc;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Order))
			return false;
		Order other = (Order) obj;
		if (this.hashCode() == other.hashCode()) {
			if (this.orderID == other.orderID) {
				if (this.date == other.date) {
					if (this.table == other.table) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "Order - orderID=" + orderID + ", date=" + date + ", tableNumber=" + table + " ";
	}

}
