package business_layer;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import data_layer.FileWriterClass;
import presentation_layer.Observer;
/**
 * � extinde Observable si implementeaza interfetele Serializable si IRestaurantProcessing; clasa contine o constanta serVUID (static final long), necesara in procesele de serializare/deserializare, o lista de obiecte de tip Observer (ArrayList<Observer>), un obiect de tipul Map<Order, ArrayList<MenuItem>> pentru maparea comenzilor intr-o structura de tip hashtable, o lista de produse pentru meniu (ArrayList<MenuItem>); pe langa constructor, clasa contine o metoda care notifica toti observerii, cate o metoda pentru generarea liniilor din tabelele pentru comenzi, respectiv meniu, metoda isWellFormed() si metode de get pentru variabilele instanta; in plus, clasa descrie corpul metodelor declarate in interfata IRestaurantProcessing;
 *
 * @invariant isWellFormed()
 */
public class Restaurant extends Observable implements Serializable, IRestaurantProcessing {
	
	
	private Map<Order, List<MenuItem>> orders;
	private List<MenuItem> menu;
	private List<Observer> observers = new ArrayList<Observer>();
	private static final long serialVersionUID = 21L;
	
	public Restaurant(Map<Order, List<MenuItem>> orders, List<MenuItem> menu) {
		this.orders = orders;
		this.menu = menu;
	
	}
	protected boolean isWellFormed() {
		if (orders == null) {
			return false;
		}
		if (menu == null) {
			return false;
		}
		Set<Map.Entry<Order, List<MenuItem>>> set = orders.entrySet();
		for (Map.Entry<Order, List<MenuItem>> entry : set) {
			if (entry.getKey() == null || entry.getValue() == null) {
				return false;
			}
			if (!menu.containsAll(entry.getValue())) {
				return false;
			}
		}
		return true;
	}

	public List<MenuItem> getMenu() {
		return menu;
	}

	public Map<Order, List<MenuItem>> getOrders() {
		return orders;
	}

	public List<Observer> getObservers() {
		return observers;
	}

	public void notifyAllObs() {
		for (Observer obs : observers) {
			obs.update();
		}
	}

	public void addNewMenuItem(MenuItem menuItem) {
		
		assert isWellFormed();
		assert menuItem.getId() > 0 && menuItem.getName() != null
				&& menuItem.computePrice() != 0 : "ID, name or price is not valid!";
		for (int i = 0; i < menu.size(); i++) {
			assert menu.get(i).getId() != menuItem.getId() : "This ID already exists!";
		}
		int preSize = menu.size();
		menu.add(menuItem);
		assert isWellFormed();
		assert menu.size() == preSize + 1;
	}

	public void editMenuItemName(int ID, String newName) {
	
	assert isWellFormed();
	assert ID > 0 && newName != null : "The ID or the newName is not valid!";
	assert menu.size() > 0 : "No items in the menu!";
	boolean ExistMenuItemToEdit = false;
	for (int i = 0; i < menu.size(); i++) {
		if (menu.get(i).getId() == ID) {
			ExistMenuItemToEdit = true;
		}
	}
	assert ExistMenuItemToEdit : "The item to edit is not found!";
	for (int i = 0; i < menu.size(); i++) {
		assert !menu.get(i).getName().equals(newName) : "The name already exists!";
	}
	boolean hasNotChanged = false;
	for (int i = 0; i < menu.size(); i++) {
		if (menu.get(i).getId() == ID)
			if(!(menu.get(i).getName().equals(newName)))
			   menu.get(i).setName(newName);
			else hasNotChanged = true;
		
		}
	
	assert !hasNotChanged : "No change!";
	assert isWellFormed();
}

	
	public void editMenuItemPrice(int ID, float newPrice) {
	
	assert isWellFormed();
	assert ID > 0 && newPrice > 0 : "The ID or the newPrice is not valid!";
	boolean ExistMenuItemToEdit = false;
	for (int i = 0; i < menu.size(); i++) {
		if (menu.get(i).getId() == ID) {
			ExistMenuItemToEdit = true;
		}
	}
	assert ExistMenuItemToEdit : "The item to edit is not found!";
	boolean hasNotChanged = false;
	for (int i = 0; i < menu.size(); i++) {
		if (menu.get(i).getId() == ID)
			if (menu.get(i).computePrice() != newPrice)
			    menu.get(i).setPrice(newPrice);
			else hasNotChanged=true;
		        
		
	}
	// se recalculeaza preturile, acolo unde este cazul, in urma modificarii de pret pentru produsul dorit
	for (MenuItem i : menu) {
		i.computePrice();
	}
	
	
	assert !hasNotChanged : "No change!";
	assert isWellFormed();
}
	public void deleteMenuItem(int ID) {
		
		assert isWellFormed();
		assert ID > 0;
		assert menu.size() > 0 : "No items in the menu!";
		int preSize = menu.size();
		MenuItem itemToDelete = null;
		for (MenuItem item : getMenu()) {
			if (item.getId() == ID) {
				itemToDelete = item;
			}
		}
		menu.remove(itemToDelete);
		assert isWellFormed();
		assert menu.size() == preSize - 1;
	}

	public void createNewOrder(int orderID, Date date, int table, List<MenuItem> items) {
		
		assert isWellFormed();
		assert orderID > 0 && date != null && table > 0 && items != null : "Order id/date/tableNumber/items is not valid!";
		Order ord = new Order(orderID,date,table);
		assert orders.containsKey(ord) : "Order already exists!";
		int preSize = orders.size();
		Order order = new Order(orderID, date, table);
		orders.put(order, items);
		assert orders.size() == preSize + 1;
		assert orders.containsKey(new Order(orderID, date, table));
		assert isWellFormed();
	}

	public void deleteOrder(Order order) {
		assert isWellFormed();
		assert orders.size() > 0 : "No orders!";
		int preSize = orders.size();
		assert orders.containsKey(order) : "Order does not exist!";
		orders.remove(order);
		assert orders.size() == preSize - 1;
		assert isWellFormed();
	}

	public float computePriceForOrder(Order order) {
		float tPrice = 0;
		assert isWellFormed();
		if(orders.containsKey(order)) {
			for (MenuItem item : orders.get(order)) {
				tPrice += item.computePrice();
			}
		}
		assert tPrice > 0;
		assert isWellFormed();
		return tPrice;
	}

	public void generateBill(String fileName, Order order) {
		assert isWellFormed();
		String s = "";
		if(orders.containsKey(order)) {
			s += "Bill for order nr." + order.getOrderID() + "\n" + "Date : " + order.getDate() + "\n\n" + "Table : "
					+ order.getTableNumber() + "\n";
			for (MenuItem item : orders.get(order)) {
				s += "Menu Item : " + item + "\n";
			}
		}
		s += "\n" + "TOTAL PRICE : " + computePriceForOrder(order);
		FileWriterClass fileWr = new FileWriterClass();
		fileWr.write(fileName, s);
		assert isWellFormed();
	}

	public Object[][] viewOrders() {
		Object[][] table = new Object[orders.size()][Order.class.getDeclaredFields().length + 1];
		int i = 0;
		Set<Map.Entry<Order, List<MenuItem>>> entrySet = orders.entrySet();
		for (Map.Entry<Order, List<MenuItem>> entry : entrySet) {
			for (int j = 0; j < Order.class.getDeclaredFields().length; j++) {
				try {
					Field field = Order.class.getDeclaredFields()[j];
					field.setAccessible(true);
					table[i][j] = field.get(entry.getKey());
					String s = "";
					for (MenuItem item : entry.getValue()) {
						Field field1 = MenuItem.class.getDeclaredFields()[1];
						field1.setAccessible(true);
						s += field1.get(item) + ", ";
					}
					table[i][Order.class.getDeclaredFields().length] = s;
				} catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
					e.printStackTrace();
				}

			}
			i++;
		}
		return table;
	}

	public Object[][] viewMenu() {
		Object[][] table = new Object[menu.size()][MenuItem.class.getDeclaredFields().length];
		int i = 0;
		for (MenuItem item : menu) {
			for (int j = 0; j < MenuItem.class.getDeclaredFields().length; j++) {
				try {
					Field field = MenuItem.class.getDeclaredFields()[j];
					field.setAccessible(true);
					if (j == 1 && item instanceof CompositeProduct) {
						String s = field.get(item) + ": ";
						for (MenuItem part : ((CompositeProduct) item).getProduct()) {
							Field field1 = MenuItem.class.getDeclaredFields()[1];
							field1.setAccessible(true);
							s += field1.get(part) + ", ";
							table[i][j] = s;
						}
					} else {
						table[i][j] = field.get(item);
					}
				} catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
					e.printStackTrace();
				}
			}
			i++;
		}
		return table;
	}

	
	

}