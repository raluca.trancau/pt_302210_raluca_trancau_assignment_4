package business_layer;

import java.util.ArrayList;
import java.util.List;
/**
 * - extinde clasa MenuItem.; aceasta contine in plus ca variabila instanta un obiect de tipul ArrayList<MenuItem> pentru stocarea produselor din care este alcatuit produsul compus (am folosit aici design pattern-ul Composite); pe langa constructor, clasa contine un getter si un setter pentru lista de produse si suprascrie metoda computePrice() din superclasa si, de asemenea, mai contine si metoda toString();
 * 
 *
 */
public class CompositeProduct extends MenuItem {
	private List<MenuItem> compositeProduct;

	public CompositeProduct(int ID, String name) {
		super(ID, name);
		this.compositeProduct = new ArrayList<MenuItem>();
	}

	@Override
	public float computePrice() {
		float price = 0;
		for (MenuItem item : compositeProduct) {
			price += item.computePrice();
		}
		super.setPrice(price);
		return price;
	}
	public List<MenuItem> getProduct() {
		return compositeProduct;
	}

	public void setProduct(List<MenuItem> compositeProduct) {
		this.compositeProduct = compositeProduct;
	}

	@Override
	public String toString() {
		String s = "CompositeProduct - ID=" + super.getId() + ", name=" + super.getName() + ", price="
				+ this.computePrice() + ", \n";
		for (int i = 0; i < compositeProduct.size() - 1; i++) {
			s += "      product =" + compositeProduct.get(i) + ",\n";
		}
		s += "      product =" + compositeProduct.get(compositeProduct.size() - 1) + " ";
		return s;
	}

	
}
