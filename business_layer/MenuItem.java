package business_layer;

import java.io.Serializable;
/**
 * � implementeaza interfata Serializable; contine trei variabile instanta: id-ul produsului (int), numele produsului (String) si pretul (float); clasa contine doi constructori, unul care are parametrii pentru toate variabilele instanta si celalalt contine doar parametrii pentru primele doua variabile instanta (acest constructor este creat pentru clasa CompositeProduct care nu foloseste la momentul instantierii obiectelor sale campul price, ci il calculeaza ulterior); pe langa constructori, contine metode de get si set pentru variabilele instanta si metoda computePrice();
 * 
 *
 */
public class MenuItem implements Serializable {
	private int ID;
	private String name;
	private float price;

	public MenuItem(int ID, String name, float price) {
		super();
		this.ID = ID;
		this.name = name;
		this.price = price;
	}

	public MenuItem(int ID, String name) {
		super();
		this.ID = ID;
		this.name = name;
	}

	public float computePrice() {
		return price;
	}

	public int getId() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public void setId(int ID) {
		this.ID = ID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
