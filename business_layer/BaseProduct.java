package business_layer;
/**
 * � extinde clasa MenuItem, contine un constructor si suprascrie metoda toString();
 * 
 *
 */
public class BaseProduct extends MenuItem {

	public BaseProduct(int ID, String name, float price) {
		super(ID, name, price);
	}

	@Override
	public String toString() {
		return "BaseProduct - ID=" + super.getId() + ", name=" + super.getName() + ", price=" + super.computePrice()
				+ " ";
	}

}
