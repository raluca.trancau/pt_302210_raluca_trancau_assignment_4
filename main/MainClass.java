
package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import business_layer.*;
import data_layer.RestaurantSerializator;
import presentation_layer.ControllerStart;
import presentation_layer.ViewStart;
/**
 * � nu contine variabile instanta si nici constructori; metoda statica main a acestei clase lanseaza in executie aplicatia si serializeaza si deserializeaza datele pentru restaurant.
 * 
 *
 */
public class MainClass {
	public static void main(String[] args) {
		List<MenuItem> menu=new ArrayList<MenuItem>();
		menu.add(new BaseProduct(1,"Gratar",15));
		menu.add(new BaseProduct(2,"Legume",7));
		menu.add(new BaseProduct(3,"Paste",25));
		Map<Order, List<MenuItem>> orders=new HashMap<Order, List<MenuItem>>();
		List<MenuItem> products=new ArrayList<MenuItem>();
		products.add(menu.get(2));
		Order ord = new Order(1,new Date(),1);
		Set<Map.Entry<Order, List<MenuItem>>> set = orders.entrySet();
		orders.put(ord, products);
		Restaurant restaurant = new Restaurant(orders, menu);
		try{
			FileOutputStream fileOut= new FileOutputStream(args[0]);
			ObjectOutputStream out= new ObjectOutputStream(fileOut);
			out.writeObject(restaurant);
			out.close(); 
			fileOut.close();
			}catch(IOException ex) {
			System.out.println("IOException is caught");
			}
		try {
			FileInputStream file = new FileInputStream("restaurant.ser");
			ObjectInputStream inputFile = new ObjectInputStream(file);
			restaurant = (Restaurant) inputFile.readObject();
			inputFile.close();
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		ViewStart view = new ViewStart();
		RestaurantSerializator ser = new RestaurantSerializator();
		ControllerStart controller = new ControllerStart(view, restaurant, ser);
	}
}

